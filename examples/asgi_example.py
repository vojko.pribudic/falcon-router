from typing import List, Tuple

import falcon
import uvicorn
from falcon.asgi.request import Request
from falcon.asgi.response import Response

from falcon_router import AppType, FalconRouter


class SuccessRes:
    async def on_get(self, _: Request, resp: Response) -> None:
        resp.status = falcon.HTTP_200
        resp.media = {"status": 200}


routes: List[Tuple] = [("/", SuccessRes())]

router: FalconRouter = FalconRouter(
    AppType.ASGI,
    route_groups="asgi_example",
    add_trailing_slash=True,
)

uvicorn.run(router.app, host="127.0.0.1", port=5000, log_level="info")
