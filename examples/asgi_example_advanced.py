import uvicorn

from falcon_router import AppType, FalconRouter

router: FalconRouter = FalconRouter(
    AppType.ASGI,
    route_groups=[
        "routes.test_routes",
        "routes.additional_routes",
    ],
    add_trailing_slash=True,
)

uvicorn.run(router.app, host="127.0.0.1", port=5000, log_level="info")
