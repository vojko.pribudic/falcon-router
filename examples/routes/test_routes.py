from typing import List, Tuple

import falcon
from falcon.asgi.request import Request
from falcon.asgi.response import Response


class TestRoutesRes:
    async def on_get(self, _: Request, resp: Response) -> None:
        resp.status = falcon.HTTP_200
        resp.media = {"status": 200, "router": "test_routes"}


routes: List[Tuple] = [("/", TestRoutesRes()), ("/test", TestRoutesRes())]
