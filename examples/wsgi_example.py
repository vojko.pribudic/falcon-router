from typing import List, Tuple

import falcon
import waitress
from falcon.request import Request
from falcon.response import Response

from falcon_router import AppType, FalconRouter


class SuccessRes:
    def on_get(self, _: Request, resp: Response) -> None:
        resp.status = falcon.HTTP_200
        resp.media = {"status": 200}


routes: List[Tuple] = [("/", SuccessRes())]

router: FalconRouter = FalconRouter(
    AppType.WSGI,
    route_groups="wsgi_example",
    add_trailing_slash=True,
)

waitress.serve(router.app, host="127.0.0.1", port=5000)
